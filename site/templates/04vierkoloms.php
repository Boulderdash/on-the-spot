<?php if ($page->kopenintro()->isNotEmpty()): ?>
        <section style="background-color: <?= $page->bground() ?>">
            <?= $page->kopenintro()->kirbytext() ?>
        </section>
<?php endif ?>

<section class="vierkoloms" style="background-color: <?= $page->bground() ?>">
    
    <div class="row">

        <div class="col-md-3 col-xs-6">

            <?= $page->linkerkolom()->kirbytext() ?>

        </div>  

        <div class="col-md-3 col-xs-6">

            <?= $page->middenlinkskolom()->kirbytext() ?>

        </div>

        <div class="col-md-3 col-xs-6">

            <?= $page->middenrechtskolom()->kirbytext() ?>

        </div>          

        <div class="col-md-3 col-xs-6">

            <?= $page->rechterkolom()->kirbytext() ?>

        </div>            

</section>    

<?php if ($page->lijn()->isNotEmpty()): ?>
    <section>
        <hr>
    </section>
<?php endif ?>      

