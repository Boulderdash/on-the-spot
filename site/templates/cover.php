<?php snippet('header') ?>

<div id="wrapper">
    <div class="wrapper-content">

    
        <section class="pagenumbers">
            <?php if ($page->prev() !== NULL): ?>
            <?php if ($page->prev()->isListed()): ?>
            

            <a class="links" href="<?= $page->prev()->url() ?>"></a>
            <?php endif ?>
            <?php endif ?>


            <?php if ($page->next() !== NULL): ?>
            <?php if ($page->next()->isListed()): ?>
            <a class="rechts" href="<?= $page->next()->url() ?>"></a>
            <?php endif ?>
            <?php endif ?>
        </section>



            <?php foreach($page->children()->listed() as $subpage): ?>

            <?= $subpage->render() ?>
        
        <?php endforeach ?>




        <section class="footer">
    
    <div class="row">

        <div class="col-md-3 col-xs-12">

        <?= $site->introtekst()->kirbytext() ?>

        </div>  

        <div class="col-md-3 col-xs-12">

        <?= $site->menuitems()->kirbytext() ?>

        </div>

        <div class="col-md-3 col-xs-12">

        <?= $site->adresgegevens()->kirbytext() ?>

        </div>          

        <div class="col-md-3 col-xs-12">

        <?= $site->overigegegevens()->kirbytext() ?>

        </div>            

</section>   

 

     </div>

<?php snippet('footer') ?>
</div>

