<?php if ($page->kopenintro()->isNotEmpty()): ?>
        <section style="background-color: <?= $page->bground() ?>">
            <?= $page->kopenintro()->kirbytext() ?>
        </section>
<?php endif ?>

<section style="background-color: <?= $page->bground() ?>">   

    <div class="row tekstblok">

        <div class="col-md-6 col-xs-12">

            <?= $page->linkerkolom()->kirbytext() ?>

        </div>  

        <div class="col-md-6 col-xs-12">

            <?= $page->rechterkolom()->kirbytext() ?>

        </div>

</section> 
        
<?php if ($page->lijn()->isNotEmpty()): ?>
    <section>
        <hr>
    </section>
<?php endif ?>            

    