<?php snippet('header') ?>

<div class="loginscherm-wrapper">
<div class="loginscherm">

<h1><?= $page->title()->html() ?></h1>

<?php if($error): ?>
<div class="alert"><?= $page->alert()->html() ?></div>
<?php endif ?>

<form method="post" action="<?= $page->url() ?>">
  <div>
    <label for="email"><?= $page->username()->html() ?></label>
    <input type="email" id="email" name="email" value="<?= esc(get('email')) ?>">
  </div>
  <div>
    <label for="password"><?= $page->password()->html() ?></label>
    <input type="password" id="password" name="password" value="<?= esc(get('password')) ?>">
  </div>
  <div>
    <input class="knop" type="submit" name="login" value="<?= $page->button()->html() ?>">
  </div>
</form>

</div>
</div>

<style>
.loginscherm-wrapper {
    display: flex;
    width: 100vw;
    height: 100vh;
    justify-content: center;
    align-items: center;
    text-align: center;
    
}

.loginscherm {
    background-color: white;
    width: 300px;
    height: 300px;
    display: block;
    padding: 30px 10px;

}

.loginscherm h1 {
    padding: 0;
    margin: 0;
    margin-bottom: 30px;
}

.loginscherm label {
    padding-bottom: 2px;
    padding-top: 10px;
    display: block;
}

.loginscherm input {
    padding: 10px;
    border: 0px;
    display: block;
    width: 100%;
}

.loginscherm .knop {
    background-color: red;
    color: white;
    text-transform: uppercase;
    cursor: pointer;
}

.loginscherm .knop:hover {
    background-color: black;
}


</style>

<?php snippet('footer') ?>