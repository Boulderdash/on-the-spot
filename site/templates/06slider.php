
<section>
    
    <ul class="lightslider">
            <?php foreach($page->images() as $image): ?>
                <li>
                    <img src="<?= $image->focusCrop(1200, 675)->url() ?>">
                </li>
            <?php endforeach ?>
    </ul>

    <style>
        .lightslider img {
            width: 100%;
        }
    </style>
 
</section>    

