<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Het e-zine gaat online over...</title>

    <?= css([
        'assets/css/normalize.css',
        'assets/css/flexboxgrid.css',
        'assets/css/socialshare.css',
        'assets/css/lightslider.min.css',
        'assets/css/lightgallery.min.css',
        'assets/css/hamburgers.css',
        'assets/css/style.css'
        ]) ?>

    <?= js([
        'assets/js/jquery.min.js',
        'assets/js/lightslider.min.js',
        'assets/js/lightgallery.min.js'
        ]) ?>

</head>

<style>

    html {
        background-color: white;
    }
    

    .wrapper {
        max-width: 1000px;
        margin: 0 auto;
        
    }

    .centered {
        height: 100vh - 100px;
        margin-top: 100px;
    }

    .titel {
        font-size: 24px;
        color: black;
        font-family: Arial, Helvetica, sans-serif;
        text-align: center;
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .klok {
       width: 100%
        height: auto;
        background-color: white;
        padding: 20px;
    }

    .klokticks {
        background-color: #17549e;
        padding: 20px;
        margin: 4px;
        width: 100%;
    }

    .klokcontainer {
        display: flex;
        align-items: stretch;
        text-align: center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 2em;
        font-weight: bold;
        color: white;
    }

    .tekst  {
        font-size: 12px;
        color: black;
        font-weight: 300;
        width: 100%;
    }    

    .slider {
        width: 100%;
        height: 250px;
        background-color: #888;
    }



    @media screen and (min-width:450px) {

    html {
        background-color: #00a2d6;
    }

    .klok {
       max-width: 800px;
        height: auto;
        background-color: white;
        padding: 20px;
    }

    .centered {
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        margin-top: 0px;
    }
    }
    
    }

</style>

<body>
    
    <div class="wrapper">

    <div id="onzichtbaar"><?= $page->onlinedag()->toDate() ?></div>
    
 
        <div class="klok centered">
                <div id="lightslider">
                        <?php foreach($page->images() as $image): ?>
                            <img src="<?= $image->url() ?>">
                        <?php endforeach ?>
                </div>
                <div id="titel" class="titel">Het e-zine komt online over:</div>
                
                
                <div class="klokcontainer">
                        <div id="days" class="klokticks">10</div>
                        <div id="hours" class="klokticks">22</div>
                        <div id="minutes" class="klokticks">33</div>
                        <div id="seconds" class="klokticks">44</div>
                </div>
                <div class="klokcontainer">
                        <div class="tekst">dagen</div>
                        <div class="tekst">uren</div>
                        <div class="tekst">minuten</div>
                        <div class="tekst">seconden</div>
                </div>
        
        </div>
    </div>



<?php snippet('footer') ?>   

