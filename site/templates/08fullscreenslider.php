<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?= $site->title() ?></title>

        <?= css([
        'assets/css/normalize.css',
        'assets/css/flexboxgrid.css',
        'assets/css/socialshare.css',
        'assets/css/lightslider.min.css',
        'assets/css/lightgallery.min.css',
        'assets/css/hamburgers.css',
        'assets/css/menu.css',
        'assets/css/animate.css',
        'assets/css/style.css'
        ]) ?>

        <?= js([
        'assets/js/jquery.min.js',
        'assets/js/jquery.chocolate.js'
        ]) ?>

        <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,700&display=swap" rel="stylesheet">
       
        <style>
    /* Full screen background slider */
        html, body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
        }

        body {
            background-position: center center;
            background-attachment: fixed;
            background-repeat: repeat;
            background-size: cover;
            z-index: -10;
        }
    </style>

        
    
    </head>
<body>

        <div class="container_introtekst">
            <div class="introtekst">
                Op 30 oktober 2019 vond in Utrecht voor de vijfde keer het NRO-congres plaats. Met dertig workshops, inspirerende sprekers, prijswinnaars en meer deelnemers dan ooit. In dit e-zine leest u over de opbrengst.
            </div> 
        </div>
 


    <div class="container_logofrontheader">  
            <div class="logofrontheader">
            <img src="assets/img/samenkoers.png" alt="">
            </div>
            
    </div>  

    <div class="lees">
        <a href="voorwoord">
        Lees dit e-zine
        </a>
    </div>



    <script>
    // full screen backgroundslider
        $('body').chocolate({
            images		: ['assets/img/voorslide1.jpg', 'assets/img/voorslide2.jpg','assets/img/voorslide3.jpg'],
            interval	: 4000,
            speed		: 2000,
        });
    </script>

    </body>
    </html>

  

