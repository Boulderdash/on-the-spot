<!-- Slider main container -->
<div class="swiper-container">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->


        <?php foreach ($items as $item): ?>

          <div class="swiper-slide" data-hash="<?= $item->titel()->text() ?>">
              <?= $item->introtekst()->kirbytext() ?>
              <div class="row">
        
                <div class="col-md-6 col-xs-12 marges">

                    <?= $item->kolomlinks()->kirbytext() ?>

                </div>  

                <div class="col-md-6 col-xs-12 marges">

                    <?= $item->kolomrechts()->kirbytext() ?>

                </div>

              </div> 
          </div>

          

          
        <?php endforeach ?>
    </div>

 

    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>

    <!-- If we need scrollbar -->

</div>

<script>
var mySwiper = new Swiper ('.swiper-container', {
    direction: 'horizontal',
    loop: false,
    slideToClickedSlide: false,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'fraction'
      },
    keyboard: {
      enabled: true,
      onlyInViewport: false
    },
    hashNavigation: {
      watchState: true
    },
    autoHeight: true
  })

  </script>