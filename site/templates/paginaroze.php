<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?= $site->title() ?></title>

        <?= css([
        'assets/css/style.css'
        ]) ?>

        <?= js([
        'assets/js/jquery.min.js',
        'assets/js/lightslider.min.js',
        'assets/js/lightgallery.min.js',
        'assets/js/jquery.chocolate.js'
        ]) ?>

        <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,700&display=swap" rel="stylesheet">
       
    </head>
<body>

    <div id="wrapper" style="background-color: #2e6295;">

        <?php foreach($page->children()->listed() as $subpage): ?>

            <?= $subpage->render() ?>
        
        <?php endforeach ?>



        <section class="leesezine">
            <?php if ($page->next()->isListed()): ?>
            <a href="<?= $page->next()->url() ?>">Lees het e-zine</a>
            <?php endif ?>
        </section>


    </div>



<?php snippet('footer') ?>


<style>

.leesezine {
    text-align: right;
    position: fixed;
    right: 15px;
    bottom: 15px;
}

.leesezine a {
    background-color: black;
    padding: 10px;
    text-decoration: none;
    color: white;
    font-size: 1.6em;
}

.leesezine a:hover {
    background-color: #999;
    color: white;
}

</style>