<div class="pagina_knoppen">


<?php if ($page->hasPrev()): ?>
<a href="<?= $page->prev()->url() ?>">Vorige
<?php endif ?>

<?php if ($page->hasNext()): ?>
<a href="<?= $page->next()->url() ?>">Volgende
<?php endif ?>

</div>