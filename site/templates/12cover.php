

<?php snippet('header') ?>

<div class="achtergronden">
    <div id="pdf" class="fixedcontainer bg_pdf"></div>
    <div id="ezine" class="fixedcontainer bg_ezine"></div>
    <div id="swiper" class="fixedcontainer bg_swiper"></div>
    <div id="time" class="fixedcontainer bg_time"></div>
    <div id="screen" class="fixedcontainer bg_screen"></div>
    <div id="meer" class="fixedcontainer bg_meer"></div>
    <div id="coverbeeld" class="fixedcontainer bg_coverbeeld"></div>
</div>



<div id="wrapper">
    <div class="wrapper-content">


        <a href="<?= $site->url() ?>">
            <img class="logo-on-the-spot" src="assets/img/logo-on-the-spot.png" alt="logo">  
        </a>
 


<a name="over">
<section id="voorwoord" class="voorwoord">
    <?= $site->introtekst()->kirbytext() ?> 
</section>


<section style="background-color: <?= $page->bground() ?>">   

    <div class="coverbeelden row">

        <?php foreach ($items as $item): ?>

            <?php if ($item->title() != 'home'): ?>
                <div class="cover-marges col-md-4 col-xs-6">
                    <a class="covercirkels" href="<?= $item->url() ?>"><img src="assets/img/knoppen/<?= $item->cirkel() ?>"></a>
          
                </div> 
            <?php endif ?>

        <?php endforeach; ?>

    </div>

</section>    

<script src="assets/js/on-the-spot-cover.js"></script>

<?php snippet('footer') ?>
</div>
</div> 




