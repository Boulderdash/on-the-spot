<?php return [
  'home' => 'inleiding',
  'debug' => true,
  'medienbaecker.autoresize.maxWidth' => 1200,
  'routes' => [
    [
      'pattern' => 'logout',
      'action'  => function() {

        if ($user = kirby()->user()) {
          $user->logout();
        }

        go('login');

      }
    ]
  ]
];


?>