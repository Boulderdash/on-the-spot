<?php
$items = $pages->listed();
if($items->isNotEmpty()):
?>

<div class="wrapper-menu-popup"> </div>   
    <div class="menu-popup centered schaduw">
        <div class="sluitknop" id="sluitknop"></div>
        <div class="menu-popup-links">

     
                <?php foreach($items as $item): ?>
                <a<?php e($item->isOpen(), ' class="active"') ?> href="<?= $item->url() ?>"><?= $item->title()->html() ?></a>
                <?php endforeach ?>
          
        </div>
        <div class="menu-popup-rechts blauw">
            <img src="assets/img/menuplaat.png" alt="">
    </div>
</div>

<?php endif ?>

<script>



</script>