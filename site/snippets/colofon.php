<a name="colofon">
<section class="mkb-colofon">
    <?= $site->colofon()->text()->kirbyText() ?>

    <div class="colofon-hoekje">
        <img src="assets/img/hoekje.png" alt="decoratief element">
    </div>

    <!-- <div class="colofon-logo">
        <img src="assets/img/detail.png" alt="logo in footer">
    </div> -->

</section>



<style>

    .mkb-colofon {
        position: relative;
        margin-top: 50px;
    }

    .colofon-hoekje {
        width: 70px;
        height: 70px;
        position: absolute;
        top: 0;
        right: 0;
    }

    .colofon-logo {
        width: 200px;
        position: absolute;
        bottom: 30px;
        right: 40px;
    }


    .colofon-hoekje img,
    .colofon-logo img {
        width: 100%;
    }

    @media screen and (max-width: 600px) {
        .colofon-logo {
        display: none;
    }
}
</style>

