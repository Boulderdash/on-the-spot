<?php if ($kirby->user() != true): ?>

<p class="reg">Alleen toegankelijk voor geregisteerde gebruikers.</p>

<style>
.reg {
    padding: 20px;
    color: white;
}

</style>

<?php endif ?>