
var achtergrondAfbeeldingen = [
    'pdf','ezine','swiper','time','screen','meer'
];

var knoppen = [
    'pdf-knop.png','ezine-knop.png','swipe-knop.png','time-knop.png','screen-knop.png','meer-knop.png'
]

var knoppenHover = [
    'pdf-knop_hover.png','ezine-knop_hover.png','swipe-knop_hover.png','time-knop_hover.png','screen-knop_hover.png','meer-knop_hover.png'
]



var kleuren = [
    'black','black','black','white','white','black'
]

var spotKnoppen = document.getElementsByClassName('covercirkels');
for (var i=0; i<spotKnoppen.length; i++) {
    var image = spotKnoppen[i].querySelector('img');
    // image.src = 'assets/img/knoppen/'+knoppen[i];
    var soortAfbeelding = document.getElementById(achtergrondAfbeeldingen[i])
    actieveKnoppen(spotKnoppen[i],soortAfbeelding, kleuren[i], knoppenHover[i]);
    actieveKnoppenMouseout(spotKnoppen[i],knoppen[i]);
};

function veranderAchtergrond(soortAfbeelding) {
    var achtergrondenDiv = document.querySelector('.achtergronden');
    achtergrondenDiv.insertBefore(soortAfbeelding, achtergrondenDiv.lastChild);
};

function actieveKnoppen(spotKnop, soortAfbeelding, kleur, spotknopHover) {
    spotKnop.addEventListener('mouseover', function() {

        var image = spotKnop.querySelector('img');
        image.src = 'assets/img/knoppen/'+spotknopHover;
        
    veranderAchtergrond(soortAfbeelding);

   

    TweenMax.to(soortAfbeelding, 0, {opacity: 0});
    TweenMax.to(soortAfbeelding, 1, {opacity: 1});
    veranderTekstWit('voorwoord',kleur);
});
};

function veranderTekstWit(elementId, waarde) {
    var deTekst = document.getElementById(elementId);
    var dieperElement = deTekst.querySelector('strong')
    dieperElement.style.color = waarde;
}





function actieveKnoppenMouseout(knop, waarde) {
    knop.addEventListener('mouseout', function() {
        var image = knop.querySelector('img');
        image.src = 'assets/img/knoppen/'+waarde;
});
}

// spotKnoppen[0].addEventListener('mouseover', function() {
//     veranderAchtergrond(pdf);
//     TweenMax.to(pdf, 0, {opacity: 0});
//     TweenMax.to(pdf, 0.5, {opacity: 1});
//     // veander img element in rollover
//     var image = spotKnoppen[0].querySelector('img');
//     console.log(image);
//     image.src = 'assets/img/knoppen/spot-fysiek-hover.png'
// })
